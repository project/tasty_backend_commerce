<?php

namespace Drupal\tasty_backend_commerce\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Defines product/add/{type} local tasks for each commerce product type.
 */
class AddProductLocalActions extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $product_types = \Drupal::entityTypeManager()->getStorage('commerce_product_type')->loadMultiple();
    foreach ($product_types as $product_type) {
      $this->derivatives['tasty_backend_commerce.product_add_form_' . $product_type->id()] = $base_plugin_definition;
      $this->derivatives['tasty_backend_commerce.product_add_form_' . $product_type->id()]['title'] = 'Add ' . mb_strtolower($product_type->label());
      $this->derivatives['tasty_backend_commerce.product_add_form_' . $product_type->id()]['route_name'] = 'entity.commerce_product.add_form';
      $this->derivatives['tasty_backend_commerce.product_add_form_' . $product_type->id()]['route_parameters']['commerce_product_type'] = $product_type->id();
      $this->derivatives['tasty_backend_commerce.product_add_form_' . $product_type->id()]['appears_on'][] = 'view.tb_commerce_manage_products_' . $product_type->id() . '.page_1';
    }

    return $this->derivatives;
  }

}
