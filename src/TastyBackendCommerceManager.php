<?php

namespace Drupal\tasty_backend_commerce;

use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\user\Entity\Role;
use Drupal\views\Views;

/**
 * Tasty Backend Commerce Manager Service.
 */
class TastyBackendCommerceManager {

  /**
   * Add a new administration view for a content type.
   *
   * @param Drupal\commerce_product\Entity\ProductType $type
   *   Commerce Product Type object.
   */
  public static function addAdminView(ProductType $type) {

    // Default view doesn't have any type set.
    $type_filter = [
      'id' => 'type',
      'table' => 'commerce_product_field_data',
      'field' => 'type',
      'value' => [
        $type->id() => $type->id(),
      ],
      'entity_type' => 'commerce_product',
      'entity_field' => 'type',
      'plugin_id' => 'commerce_entity_bundle',
      'group' => 1,
    ];

    // Duplicate the view.
    $view = Views::getView('tb_commerce_manage_products')->storage->createDuplicate();

    // Set some basic info.
    $view->setStatus(TRUE);
    $view->set('id', 'tb_commerce_manage_products_' . $type->id());
    $view->set('label', 'Tasty Backend Commerce Manage ' . $type->label());
    $view->set('description', 'Tasty Backend Commerce administration view to manage all ' . $type->label() . ' products.');

    // Set the display options.
    $display = $view->get('display');
    $display['default']['display_options']['access']['options']['perm'] = 'update any ' . $type->id() . ' commerce_product';
    $display['default']['display_options']['filters']['type'] = $type_filter;
    $display['default']['display_options']['title'] = 'Manage ' . $type->label() . ' products';
    $display['page_1']['display_options']['path'] = 'admin/manage/store/products/' . $type->id();
    $display['page_1']['display_options']['menu']['title'] = $type->label();
    $display['page_1']['display_options']['menu']['description'] = 'Manage ' . $type->label() . ' products.';
    $view->set('display', $display);

    // Save the new view.
    $view->save();

    // Set watchdog message that the new view was created.
    \Drupal::logger('tasty_backend_commerce')->notice('Created new Tasty Backend Commerce management view: @view.', ['@view' => $view->label()]);
  }

  /**
   * Add default permissions for a product type.
   *
   * @param Drupal\commerce_product\Entity\ProductType $type
   *   Drupal Commerce ProductType object.
   * @param string $rid
   *   The ID of a user role to alter.
   */
  public static function addProductTypePermissions(ProductType $type, $rid = 'tb_store_admin') {
    $role = Role::load($rid);
    user_role_grant_permissions($rid, [
      'create ' . $type->id() . ' commerce_product',
      'delete any ' . $type->id() . ' commerce_product',
      'update any ' . $type->id() . ' commerce_product',
    ]);
    $args = [
      '%role_name' => $role->label(),
      '%type' => $type->label(),
    ];
    \Drupal::messenger()->addMessage(t('Default commerce product type permissions have been added to the %role_name role for the %type product type.', $args));
  }

  /**
   * Add default permissions for a product variation type.
   *
   * @param Drupal\commerce_product\Entity\ProductVariationType $type
   *   Drupal Commerce ProductVariationType object.
   * @param string $rid
   *   The ID of a user role to alter.
   */
  public static function addProductVariationTypePermissions(ProductVariationType $type, $rid = 'tb_store_admin') {
    $role = Role::load($rid);
    user_role_grant_permissions($rid, [
      'manage ' . $type->id() . ' commerce_product_variation',
    ]);
    $args = [
      '%role_name' => $role->label(),
      '%type' => $type->label(),
    ];
    \Drupal::messenger()->addMessage(t('Default commerce product variation type permissions have been added to the %role_name role for the %type product variation type.', $args));
  }

}
